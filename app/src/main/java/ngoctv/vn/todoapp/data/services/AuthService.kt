package ngoctv.vn.todoapp.data.services

import androidx.lifecycle.LiveData
import io.reactivex.Completable
import ngoctv.vn.todoapp.data.entities.User

interface AuthService{
    fun userExists(username: String?): LiveData<Boolean>
    fun login(username: String, password: String): Completable
    fun register(username: String, password: String): Completable
    fun requireCurrentUser(): User
    fun userIsLogged(): Boolean
    fun logout()
}