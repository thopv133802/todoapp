package ngoctv.vn.todoapp.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import ngoctv.vn.todoapp.data.entities.Done
import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.data.entities.User
import ngoctv.vn.todoapp.data.repositories.AuthDao
import ngoctv.vn.todoapp.data.repositories.TaskDao
import ngoctv.vn.todoapp.utils.DATABASE_NAME

@Database(
    entities = [
        User::class,
        Task::class,
        Done::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {
    abstract fun authDao(): AuthDao
    abstract fun taskDao(): TaskDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        fun getInstance(context: Context) = instance ?: synchronized(this){
            instance ?: Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .addCallback(object: Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        val request = OneTimeWorkRequest.from(DatabaseSeeder::class.java)
                        WorkManager.getInstance().enqueue(request)
                    }
                })
                .build().also {
                    instance = it
                }
        }
    }
}

class DatabaseSeeder(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    override fun doWork(): Result {
        val database: AppDatabase = AppDatabase.getInstance(applicationContext)
        val user = User.newUser()

        database.authDao().saveUser(user)
        for(i in 1..10){
            val task = Task.newTask(user.username)
            database.taskDao().saveTask(task)
        }

        return Result.success()
    }

}