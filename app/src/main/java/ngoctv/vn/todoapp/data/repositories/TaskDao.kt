package ngoctv.vn.todoapp.data.repositories

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction
import ngoctv.vn.todoapp.data.entities.Done
import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.data.models.TasksTaskModel

@Dao
abstract class TaskDao{
    @Query("""
        select
            tasks.id as id,
            tasks.name as name,
            tasks.description as description,
            tasks.address as address,
            tasks.deadline as deadline,
            tasks.created as created,
            tasks.userId as userId,
            tasks.isDone as isDone
        from tasks
        where
            tasks.userId = :userId
        order by isDone, deadline
    """)
    abstract fun fetchTasksDataSource(userId: String): DataSource.Factory<Int, TasksTaskModel>

    @Query(
        "select * from tasks"
    )
    abstract fun fetchLiveTasks(): LiveData<List<Task>>

    @Insert(onConflict = REPLACE)
    abstract fun saveTask(task: Task)

    @Query("""
        update tasks
        set isDone = :done
        where id = :taskId
    """)
    abstract fun setDone(taskId: String, done: Boolean)

    @Insert(onConflict = REPLACE)
    abstract fun saveDone(done: Done)

    @Query("delete from dones where taskId = :taskId")
    abstract fun removeDone(taskId: String)

    @Query("select * from tasks where id = :taskId")
    abstract fun fetchLiveTask(taskId: String): LiveData<Task>

    @Query("""
        select
            tasks.id as id,
            tasks.name as name,
            tasks.description as description,
            tasks.address as address,
            tasks.deadline as deadline,
            tasks.created as created,
            tasks.userId as userId,
            tasks.isDone as isDone
        from tasks
        where
            tasks.userId = :userId
            and tasks.isDone = :isDone
        order by deadline
    """)
    abstract fun fetchTasksDataSource(userId: String, isDone: Boolean): DataSource.Factory<Int, TasksTaskModel>

}