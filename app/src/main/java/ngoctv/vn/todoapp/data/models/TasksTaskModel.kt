package ngoctv.vn.todoapp.data.models

import androidx.room.Ignore
import androidx.room.Relation
import ngoctv.vn.todoapp.data.entities.Done

data class TasksTaskModel(
    var id: String,
    var name: String,
    var description: String,
    var address: String,
    var deadline: Long,
    var created: Long,
    var userId: String,
    var isDone: Boolean
)