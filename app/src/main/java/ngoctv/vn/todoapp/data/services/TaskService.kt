package ngoctv.vn.todoapp.data.services

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import io.reactivex.Completable
import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.data.models.TasksTaskModel

interface TaskService {
    fun fetchLiveTasks(userId: String, pageSize: Int, filter: Task.Filter = Task.Filter.NoFilter): LiveData<PagedList<TasksTaskModel>>
    fun setDone(taskId: String, isDone: Boolean = true): Completable
    fun fetchLiveTask(taskId: String): LiveData<Task>
    fun saveTask(task: Task): Completable
}