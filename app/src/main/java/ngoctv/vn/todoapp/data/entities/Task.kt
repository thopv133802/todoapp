package ngoctv.vn.todoapp.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import ngoctv.vn.todoapp.utils.DateUtils
import ngoctv.vn.todoapp.utils.currentTimeInMillis
import ngoctv.vn.todoapp.utils.newUUID
import java.util.*

@Entity(
    tableName = "tasks",
    foreignKeys = [
        ForeignKey(
            entity = User::class,
            parentColumns = ["username"],
            childColumns = ["userId"]
        )
    ],
    indices = [
        Index("userId")
    ]
)
data class Task(
    @PrimaryKey
    val id: String,
    val name: String,
    val description: String,
    val address: String,
    val deadline: Long,
    val created: Long,
    val userId: String,
    val isDone: Boolean
){

    companion object {
        fun newTask(userId: String): Task{
            return Task(
                id = newUUID(),
                name = "Tên công việc",
                description =  "Miêu tả nội dung công việc",
                address = "Địa chỉ",
                deadline =  currentTimeInMillis() + 3600,
                created = currentTimeInMillis(),
                userId = userId,
                isDone =  false
            )
        }
    }
    constructor(name: String, description: String, address: String, date: String, time: String, userId: String): this(
        newUUID(),
        name,
        description,
        address,
        DateUtils.dateTimeToTimeInMillis(date, time),
        currentTimeInMillis(),
        userId,
        false
    )

    enum class Filter{
        NoFilter,
        Done,
        Todo
    }
}