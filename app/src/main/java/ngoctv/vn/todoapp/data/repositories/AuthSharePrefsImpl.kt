package ngoctv.vn.todoapp.data.repositories

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.google.gson.Gson
import ngoctv.vn.todoapp.data.entities.User

class AuthSharePrefsImpl(context: Context): AuthSharePrefs {
    private val sharePrefs = context.getSharedPreferences(SHARE_PREF_NAME, MODE_PRIVATE)
    override fun isLogged(): Boolean {
        return getCurrentUser() != null
    }

    override fun saveUser(user: User) {
        sharePrefs.edit()
            .putString(CURRENT_USER, Gson().toJson(user))
            .apply()
    }

    override fun getCurrentUser(): User? {
        val json = sharePrefs.getString(CURRENT_USER, null)
        return if(json != null)Gson().fromJson(json, User::class.java) else null
    }



    override fun removeCurrentUser() {
        sharePrefs
            .edit()
            .remove(CURRENT_USER)
            .apply()
    }

    companion object {
        private const val CURRENT_USER = "currentUser"
        private const val SHARE_PREF_NAME = "Auth"

        @Volatile
        private var instance : AuthSharePrefs ?= null
        fun getInstance(context: Context) = instance ?: synchronized(this){
            instance ?: AuthSharePrefsImpl(context).also {
                instance = it
            }
        }
    }
}