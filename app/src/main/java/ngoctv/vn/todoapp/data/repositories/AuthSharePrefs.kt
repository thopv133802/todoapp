package ngoctv.vn.todoapp.data.repositories

import ngoctv.vn.todoapp.data.entities.User

interface AuthSharePrefs{
    fun isLogged(): Boolean
    fun saveUser(user: User)
    fun getCurrentUser(): User?
    fun removeCurrentUser()
}