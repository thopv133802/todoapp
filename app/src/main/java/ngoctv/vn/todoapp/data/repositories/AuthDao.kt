package ngoctv.vn.todoapp.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.FAIL
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import io.reactivex.Single
import ngoctv.vn.todoapp.data.entities.User

@Dao
abstract class AuthDao{
    @Query("select count(*) from users where username = :username")
    protected abstract fun liveUserCount(username: String): LiveData<Int>
    @Query("select count(*) from users where username = :username")
    protected abstract fun userCount(username: String): Int

    fun liveUserExists(username: String): LiveData<Boolean>{
        return Transformations.map(liveUserCount(username)){ count ->
            count > 0
        }
    }

    @Query("select * from users where username = :username")
    abstract fun fetchUser(username: String): Single<User?>

    fun userExists(username: String): Boolean{
        return userCount(username) > 0
    }

    @Insert(onConflict = IGNORE)
    abstract fun saveUser(user: User)

}