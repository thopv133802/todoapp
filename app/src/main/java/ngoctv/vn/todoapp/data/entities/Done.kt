package ngoctv.vn.todoapp.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "dones",
    foreignKeys = [
        ForeignKey(
            entity = Task::class,
            parentColumns = ["id"],
            childColumns = ["taskId"]
        )
    ],
    indices = [
        Index("taskId")
    ]
)
data class Done(
    @PrimaryKey
    val taskId: String,
    val created: Long
)