package ngoctv.vn.todoapp.data.services

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ngoctv.vn.todoapp.data.entities.User
import ngoctv.vn.todoapp.data.repositories.AuthDao
import ngoctv.vn.todoapp.data.repositories.AuthSharePrefs
import java.lang.Exception
import java.lang.RuntimeException

class AuthServiceImpl(private val authDao: AuthDao, private val authSharePrefs: AuthSharePrefs) : AuthService {
    override fun logout() {
        authSharePrefs.removeCurrentUser()
    }

    override fun userIsLogged(): Boolean {
        return authSharePrefs.isLogged()
    }

    override fun requireCurrentUser(): User {
        return authSharePrefs.getCurrentUser() ?: throw RuntimeException("Người dùng hiện tại không khả dụng")
    }

    override fun userExists(username: String?): LiveData<Boolean> {
        return when{
            username.isNullOrBlank() -> MutableLiveData<Boolean>().apply{
                value = false
            }
            else -> authDao.liveUserExists(username)
        }
    }

    override fun login(username: String, password: String): Completable {
        return authDao.fetchUser(username)
            .subscribeOn(Schedulers.io())
            .flatMapCompletable { user: User? ->
                if(user == null)
                    Completable.error(Exception("Người dùng không tồn tại"))
                else if(user.password != password)
                    Completable.error(Exception("Mật khẩu không chính xác"))
                else
                    Completable.fromAction {
                        authSharePrefs.saveUser(user)
                    }
            }
            .onErrorResumeNext {
                Completable.error(Exception("Người dùng không tồn tại"))
            }
    }

    override fun register(username: String, password: String): Completable {
        return Single.fromCallable {
            authDao.userExists(username)
        }
            .subscribeOn(Schedulers.io())
            .flatMapCompletable {exist ->
                if(exist)
                    Completable.error(Exception("Người dùng đã tồn tại"))
                else
                    Completable.fromAction {
                        authDao.saveUser(
                            User(
                                username,
                                password
                            )
                        )
                    }
            }
    }

    companion object {
        @Volatile
        private var instance: AuthService ?= null

        fun getInstance(authDao: AuthDao, authSharePrefs: AuthSharePrefs) = instance ?: synchronized(this){
            instance ?: AuthServiceImpl(authDao, authSharePrefs).also {
                instance = it
            }
        }

    }
}