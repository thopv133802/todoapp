package ngoctv.vn.todoapp.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "users")
data class User(
    @PrimaryKey
    val username: String,
    val password: String
){
    companion object {
        fun newUser(): User{
            return User(
                "username",
                "password"
            )
        }
    }
}