package ngoctv.vn.todoapp.data.services

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import ngoctv.vn.todoapp.data.entities.Done
import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.data.models.TasksTaskModel
import ngoctv.vn.todoapp.data.repositories.TaskDao
import ngoctv.vn.todoapp.utils.currentTimeInMillis

class TaskServiceImpl (private val taskDao: TaskDao): TaskService {
    override fun fetchLiveTasks(
        userId: String,
        pageSize: Int,
        filter: Task.Filter
    ): LiveData<PagedList<TasksTaskModel>> {
        val dataSource = when(filter){
            Task.Filter.NoFilter -> taskDao.fetchTasksDataSource(userId)
            Task.Filter.Done -> taskDao.fetchTasksDataSource(userId, isDone = true)
            Task.Filter.Todo -> taskDao.fetchTasksDataSource(userId, isDone = false)
        }
        return LivePagedListBuilder(
            dataSource,
            pageSize
        ).build()
    }

    override fun saveTask(task: Task): Completable {
        return Completable.fromAction {
            taskDao.saveTask(task)
        }
            .subscribeOn(Schedulers.io())
    }

    override fun fetchLiveTask(taskId: String): LiveData<Task> {
        return taskDao.fetchLiveTask(taskId)
    }

    override fun setDone(taskId: String, isDone: Boolean): Completable {
        return Completable.fromAction {
            taskDao.setDone(taskId, isDone)
            if(isDone)
                taskDao.saveDone(
                    Done(
                        taskId,
                        currentTimeInMillis()
                    )
                )
            else
                taskDao.removeDone(
                   taskId
                )
        }
            .subscribeOn(Schedulers.io())
    }



    companion object {
        @Volatile
        private var instance: TaskService ?= null

        fun getInstance(taskDao: TaskDao) = instance ?: synchronized(this){
            instance ?: TaskServiceImpl(
                taskDao
            ).also {
                instance = it
            }
        }
    }
}