package ngoctv.vn.todoapp.data.models

import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.utils.DateUtils
import ngoctv.vn.todoapp.utils.toDateString
import ngoctv.vn.todoapp.utils.toTimeString

data class EditableTaskModel(
    var id: String,
    var name: String,
    var description: String,
    var address: String,
    var date: String,
    var time: String,
    var created: Long,
    var userId: String,
    var isDone: Boolean
)

fun Task.toEditable(): EditableTaskModel{
    return EditableTaskModel(
        id,
        name,
        description,
        address,
        deadline.toDateString(),
        deadline.toTimeString(),
        created,
        userId,
        isDone
    )
}
@Throws(Exception::class)
fun EditableTaskModel.toEntity(): Task{
    return Task(
        id,
        name,
        description,
        address,
        DateUtils.dateTimeToTimeInMillis(date, time),
        created,
        userId,
        isDone
    )
}