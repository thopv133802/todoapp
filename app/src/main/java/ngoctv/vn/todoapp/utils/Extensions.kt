package ngoctv.vn.todoapp.utils

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import java.lang.RuntimeException
import java.lang.StringBuilder
import java.util.*
import java.util.Calendar.*

fun Long.toDatetimeString(): String{
    val result = StringBuilder()

    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this

    result.append(toTimeString())

    result.append(" | ")

    result.append(toDateString())

    return result.toString()
}

fun currentTimeInMillis() = System.currentTimeMillis()
fun currentDateString(): String{
    return currentTimeInMillis().toDateString()
}
fun currentTimeString(): String{
    return currentTimeInMillis().toTimeString()
}
fun newUUID() = UUID.randomUUID().toString()

fun Context.buildToast(message: String): Toast{
    return Toast.makeText(this, message, Toast.LENGTH_SHORT)
}
fun View.buildSnackbar(message: String): Snackbar{
    return Snackbar.make(this, message, Snackbar.LENGTH_SHORT)
}
fun View.showSnackbar(message: String){
    buildSnackbar(message).show()
}
fun Context.showToast(message: String){
    buildToast(message).show()
}

fun Long.toDateString(): String{
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    val result = StringBuilder()
    result.append(calendar.get(DAY_OF_MONTH))
    result.append("/")
    result.append(calendar.get(MONTH )+ 1)
    result.append("/")
    result.append(calendar.get(YEAR))

    Log.e("ToDateString", "Value = " + result.toString())

    return result.toString()
}
fun Long.toTimeString(): String{
    val result = StringBuilder()

    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this

    result.append(calendar.get(HOUR_OF_DAY))
    result.append(":")
    result.append(calendar.get(MINUTE))

    Log.e("ToTimeString", "Value = " + result.toString())

    return result.toString()
}
object DateUtils{
    @Throws(Exception::class)
    fun dateTimeToTimeInMillis(date: String, time: String): Long{
        val dates = date.split("/")
        if(dates.size != 3)
            throw Exception("Sai mẫu, Mẫu đúng là: Ngày/Tháng/Năm")
        val times = time.split(":")
        if(times.size != 2)
            throw Exception("Sai mẫu, Mẫu đúng là: Template: Giờ:Phút")
        val calendar = Calendar.getInstance()

        calendar.set(HOUR_OF_DAY, times[0].toInt())
        calendar.set(MINUTE, times[1].toInt())

        calendar.set(DAY_OF_MONTH, dates[0].toInt())
        calendar.set(MONTH, dates[1] .toInt() - 1)
        calendar.set(YEAR, dates[2].toInt())

        return calendar.timeInMillis
    }

    fun validateDateError(date: String): String? {
        val dates = date.split("/")
        if(dates.size != 3)
            return "Sai mẫu, Mẫu đúng là: Ngày/Tháng/Năm"
        return null
    }

    fun validateTimeError(time: String): String? {
        val times = time.split(":")
        if(times.size != 2)
            return "Sai mẫu, Mẫu đúng là: Template: Giờ:Phút"
        return null
    }
}