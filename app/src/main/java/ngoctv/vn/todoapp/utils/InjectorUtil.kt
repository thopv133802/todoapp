package ngoctv.vn.todoapp.utils

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import ngoctv.vn.todoapp.data.AppDatabase
import ngoctv.vn.todoapp.data.repositories.AuthDao
import ngoctv.vn.todoapp.data.repositories.AuthSharePrefsImpl
import ngoctv.vn.todoapp.data.services.AuthService
import ngoctv.vn.todoapp.data.services.AuthServiceImpl
import ngoctv.vn.todoapp.data.services.TaskService
import ngoctv.vn.todoapp.data.services.TaskServiceImpl
import ngoctv.vn.todoapp.ui.viewModels.addtask.AddTaskViewModel
import ngoctv.vn.todoapp.ui.viewModels.auth.LoginViewModel
import ngoctv.vn.todoapp.ui.viewModels.auth.RegisterViewModel
import ngoctv.vn.todoapp.ui.viewModels.edittask.EditTaskViewModel
import ngoctv.vn.todoapp.ui.viewModels.tasks.TasksViewModel

object InjectorUtil{
    fun appDatabase(context: Context): AppDatabase = AppDatabase.getInstance(context)
    fun authDao(context: Context): AuthDao = appDatabase(context).authDao()
    fun authSharePrefs(context: Context) = AuthSharePrefsImpl.getInstance(context)
    fun authService(context: Context): AuthService = AuthServiceImpl.getInstance(authDao(context), authSharePrefs(context))
    fun loginViewModelFactory(context: Context): LoginViewModel.Factory  = LoginViewModel.Factory(authService(context))
    fun registerViewModelFactory(context: Context): ViewModelProvider.Factory = RegisterViewModel.Factory(authService(context))

    fun taskDao(context: Context) = AppDatabase.getInstance(context).taskDao()

    fun taskService(context: Context): TaskService = TaskServiceImpl.getInstance(taskDao(context))
    fun tasksViewModelFactory(context: Context): TasksViewModel.Factory = TasksViewModel.Factory(authService(context), taskService(context))
    fun editTaskViewModelFactory(context: Context) = EditTaskViewModel.Factory(taskService(context))
    fun addTaskViewModelFactory(context: Context) = AddTaskViewModel.Factory(taskService(context), authService(context))
}