package ngoctv.vn.todoapp.utils

fun validateTime(time: String?): String? {
    return when{
        time.isNullOrBlank() -> "Giờ không được để trống"
        else -> DateUtils.validateTimeError(time)
    }
}

fun validateDate(date: String?): String? {
    return when{
        date.isNullOrBlank() -> "Ngày không được để trống"
        else -> DateUtils.validateDateError(date)
    }
}

fun validateAddress(address: String?): String? {
    return when{
        address.isNullOrBlank() -> "Địa điểm không được để trống"
        else -> null
    }
}

fun validateDescription(description:  String?): String? {
    return when{
        description.isNullOrBlank() -> "Nội dung không được để trống"
        else -> null
    }
}

fun validateName(name: String?): String? {
    return when{
        name.isNullOrBlank() -> "Tên không được để trống"
        else -> null
    }
}