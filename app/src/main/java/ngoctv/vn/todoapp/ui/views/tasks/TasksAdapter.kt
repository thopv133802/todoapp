package ngoctv.vn.todoapp.ui.views.tasks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ngoctv.vn.todoapp.R
import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.data.models.TasksTaskModel
import ngoctv.vn.todoapp.databinding.TasksTaskCardBinding
import ngoctv.vn.todoapp.ui.viewModels.tasks.TasksTaskViewModel
import ngoctv.vn.todoapp.utils.InjectorUtil
import ngoctv.vn.todoapp.utils.buildSnackbar
import ngoctv.vn.todoapp.utils.buildToast
import ngoctv.vn.todoapp.utils.showSnackbar
import java.lang.ref.WeakReference

class TasksAdapter(
    private val navController: NavController,
    viewLifecycleOwner: LifecycleOwner
) : PagedListAdapter<TasksTaskModel, TasksAdapter.TaskViewHolder>(object: DiffUtil.ItemCallback<TasksTaskModel>() {
    override fun areItemsTheSame(oldItem: TasksTaskModel, newItem: TasksTaskModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: TasksTaskModel, newItem: TasksTaskModel): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: TasksTaskModel, newItem: TasksTaskModel): Any? {
        return setOf(
            TasksTaskModel::isDone
        )
    }
}){
    private val viewLifecycleOwnerRef = WeakReference(viewLifecycleOwner)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val binding: TasksTaskCardBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.tasks_task_card,
            parent,
            false
        )
        binding.setLifecycleOwner(viewLifecycleOwnerRef.get())
        return TaskViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        getItem(position)?.let{task ->
            holder.bind(task, navController)
        }
    }

    class TaskViewHolder(private val binding: TasksTaskCardBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(task: TasksTaskModel, navController: NavController) {
            viewModel.setTask(task)
            binding.edit.setOnClickListener {
                navController.navigate(
                    R.id.action_tasksFragment_to_editTaskFragment,
                    Bundle().apply{
                        putString("taskId", task.id)
                    }
                )
            }
        }

        private val viewModel: TasksTaskViewModel = TasksTaskViewModel(InjectorUtil.taskService(binding.root.context))
        init{
            binding.viewModel = viewModel
        }

    }
}