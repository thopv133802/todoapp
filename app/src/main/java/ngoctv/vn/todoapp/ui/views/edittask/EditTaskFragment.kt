package ngoctv.vn.todoapp.ui.views.edittask


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import ngoctv.vn.todoapp.R
import ngoctv.vn.todoapp.databinding.EditTaskFragmentBinding
import ngoctv.vn.todoapp.ui.viewModels.edittask.EditTaskViewModel
import ngoctv.vn.todoapp.utils.InjectorUtil
import ngoctv.vn.todoapp.utils.showSnackbar


/**
 * A simple [Fragment] subclass.
 *
 */
class EditTaskFragment : Fragment() {
    lateinit var taskId: String
    lateinit var binding: EditTaskFragmentBinding
    lateinit var viewModel: EditTaskViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        input()
        initViewModel()
        initBinding(inflater, container)
        setupViews()
        return binding.root
    }

    private fun setupViews() {
        binding.update.setOnClickListener {
            viewModel.update {message ->
                binding.root.showSnackbar(message)
            }
        }
    }

    private fun input() {
        taskId = arguments!!.getString("taskId")!!
    }

    private fun initViewModel() {
        val factory = InjectorUtil.editTaskViewModelFactory(context = requireContext())
        viewModel = ViewModelProviders.of(this, factory).get(EditTaskViewModel::class.java)
        viewModel.setTaskId(taskId)
    }

    private fun initBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, R.layout.edit_task_fragment, container, false)
        binding.setLifecycleOwner(viewLifecycleOwner)
        binding.viewModel = viewModel
    }


}
