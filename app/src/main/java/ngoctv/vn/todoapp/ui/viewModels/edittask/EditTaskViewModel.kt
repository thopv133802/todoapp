package ngoctv.vn.todoapp.ui.viewModels.edittask

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.data.models.EditableTaskModel
import ngoctv.vn.todoapp.data.models.toEditable
import ngoctv.vn.todoapp.data.models.toEntity
import ngoctv.vn.todoapp.data.services.TaskService
import ngoctv.vn.todoapp.utils.*
import java.io.FileDescriptor

@Suppress("UNCHECKED_CAST")
class EditTaskViewModel(private val taskService: TaskService): ViewModel(){
    private val taskId = MutableLiveData<String>()
    private val task: LiveData<Task> = Transformations.switchMap(taskId){ taskId ->
        taskService.fetchLiveTask(taskId)
    }

    val name: MutableLiveData<String> = MediatorLiveData<String>().apply{
        addSource(task){task ->
            value = task.name
        }
    }
    val description: MutableLiveData<String> = MediatorLiveData<String>().apply{
        addSource(task){task ->
            value = task.description
        }
    }
    val address: MutableLiveData<String> = MediatorLiveData<String>().apply{
        addSource(task){task ->
            value = task.address
        }
    }

    val date: MutableLiveData<String> = MediatorLiveData<String>().apply{
        addSource(task){task ->
            value = task.deadline.toDateString()
        }
    }

    val time: MutableLiveData<String> = MediatorLiveData<String>().apply{
        addSource(task){task ->
            value = task.deadline.toTimeString()
        }
    }

    val showError = MutableLiveData<Boolean>().apply{
        value = false
    }

    val nameError: LiveData<String?> = Transformations.map(name){name: String? ->
        validateName(name)
    }
    val descriptionError: LiveData<String?> = Transformations.map(description){description: String? ->
        validateDescription(description)
    }
    val addressError: LiveData<String?> = Transformations.map(address){address: String? ->
        validateAddress(address)
    }

    val dateError: LiveData<String?> = Transformations.map(date){date: String? ->
        validateDate(date)
    }
    val timeError: LiveData<String?> = Transformations.map(time){time: String? ->
        validateTime(time)
    }

    val inputValid = MediatorLiveData<Boolean>().apply{
        value = true
        addSource(nameError){value = validateInput()}
        addSource(descriptionError){value = validateInput()}
        addSource(addressError){value = validateInput()}
        addSource(dateError){value = validateInput()}
        addSource(timeError){value = validateInput()}
    }

    private fun validateInput(): Boolean{
        return nameError.value == null
            && descriptionError.value == null
            && addressError.value == null
            && dateError.value == null
            && timeError.value == null
    }

    fun setTaskId(taskId: String){
        this.taskId.value = taskId
    }
    fun update(onComplete: ((String) -> Unit)?){
        showError.value = true
        val inputValid = validateInput()
        val task = task.value
        when{
            !inputValid -> onComplete?.invoke("Cập nhật thất bại: Dữ liệu không hợp lệ")
            task == null -> onComplete?.invoke("Cập nhật thất bại: Task hiện tại không khả dụng")
            true -> {
                taskService.saveTask(
                    task.copy(
                        name = name.value!!,
                        description = description.value!!,
                        address = address.value!!,
                        deadline = DateUtils.dateTimeToTimeInMillis(date.value!!, time.value!!)
                    )
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        onComplete?.invoke("Cập nhật thành công")
                    }, {
                        onComplete?.invoke("Cập nhật thất bại: " + it.message)
                    })
            }
        }
    }
    class Factory(private val taskService: TaskService): ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return EditTaskViewModel(taskService) as T
        }
    }
}