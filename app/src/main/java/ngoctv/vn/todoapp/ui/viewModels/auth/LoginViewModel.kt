package ngoctv.vn.todoapp.ui.viewModels.auth

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import ngoctv.vn.todoapp.data.services.AuthService
import java.lang.Exception

@Suppress("UNCHECKED_CAST")
class LoginViewModel(private val authService: AuthService): ViewModel(){
    var showError: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply{
        value = false
    }

    val username: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "username"
    }
    val password: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = "password"
    }

    val usernameError: LiveData<String?> = Transformations.map(username){username ->
        validateUsername(username)
    }

    private fun validateUsername(username: String?): String? {
        return when{
            username.isNullOrBlank() -> "Tài khoản không được để trống"
            else -> null
        }
    }

    val passwordError: LiveData<String?> = Transformations.map(password){password ->
        validatePassword(password)
    }

    private fun validatePassword(password: String?): String? {
        return when{
            password.isNullOrBlank() -> "Mật khẩu không được để trống"
            else -> null
        }
    }

    val inputValid: LiveData<Boolean> = MediatorLiveData<Boolean>().apply{
        value = false
        addSource(usernameError){
            value = validateInput()
        }
        addSource(passwordError){
            value = validateInput()
        }
    }

    private fun validateInput(): Boolean {
        return usernameError.value == null && passwordError.value == null
    }

    fun login(onSuccess: ((String) -> Unit)?, onFail: ((String) -> Unit)?){
        showError.value = true
        val usernameError = validateUsername(username.value) != null
        val passwordError = validatePassword(password.value) != null
        when{
            usernameError -> onFail?.invoke("Tài khoản không hợp lệ")
            passwordError -> onFail?.invoke("Mật khẩu không hợp lệ")
            else -> {
                authService.login(
                    username.value!!,
                    password.value!!
                ).observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        onSuccess?.invoke("Đăng nhập thành công")
                    }, {
                        onFail?.invoke("Đăng nhập thất bại: " + it.message)
                    })
            }
        }
    }

    class Factory(private val authService: AuthService): ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginViewModel(authService) as T
        }
    }
}