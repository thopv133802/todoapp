package ngoctv.vn.todoapp.ui.views.addtask


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import ngoctv.vn.todoapp.R
import ngoctv.vn.todoapp.databinding.AddTaskFragmentBinding
import ngoctv.vn.todoapp.ui.viewModels.addtask.AddTaskViewModel
import ngoctv.vn.todoapp.utils.InjectorUtil
import ngoctv.vn.todoapp.utils.showToast


class AddTaskFragment : Fragment() {
    lateinit var binding: AddTaskFragmentBinding
    lateinit var viewModel: AddTaskViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        initViewModel()
        initBinding(inflater, container)
        setupViews()

        return binding.root
    }

    private fun setupViews() {
        binding.add.setOnClickListener {
            viewModel.add {message ->
                requireContext().showToast(message)
            }
        }
    }

    private fun initBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, R.layout.add_task_fragment, container, false)
        binding.setLifecycleOwner(viewLifecycleOwner)
        binding.viewModel = viewModel
    }

    private fun initViewModel() {
        val factory = InjectorUtil.addTaskViewModelFactory(context = requireContext())
        viewModel = ViewModelProviders.of(this, factory).get(AddTaskViewModel::class.java)
    }


}
