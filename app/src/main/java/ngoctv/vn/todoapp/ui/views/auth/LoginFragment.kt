package ngoctv.vn.todoapp.ui.views.auth


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment.findNavController

import ngoctv.vn.todoapp.R
import ngoctv.vn.todoapp.databinding.LoginFragmentBinding
import ngoctv.vn.todoapp.ui.viewModels.auth.LoginViewModel
import ngoctv.vn.todoapp.utils.InjectorUtil

class LoginFragment : Fragment() {
    lateinit var binding: LoginFragmentBinding
    lateinit var viewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        initViewModel()
        initBinding(inflater, container)
        setupViews()
        return binding.root
    }

    private fun setupViews() {
        binding.login.setOnClickListener {
            viewModel.login({message ->
                showMessage(message)
                findNavController(this).navigate(R.id.tasksFragment)
            }, {error ->
                showMessage(error)
            })
        }
        binding.register.setOnClickListener{
            findNavController(this).navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }
    private fun showMessage(message: String){
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
            .show()
    }

    private fun initViewModel() {
        val factory = InjectorUtil.loginViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(this, factory).get(LoginViewModel::class.java)
    }

    private fun initBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)
        binding.viewModel = viewModel
        binding.setLifecycleOwner(viewLifecycleOwner)
    }
}
