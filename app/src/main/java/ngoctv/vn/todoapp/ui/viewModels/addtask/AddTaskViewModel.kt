package ngoctv.vn.todoapp.ui.viewModels.addtask

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.data.entities.User
import ngoctv.vn.todoapp.data.services.AuthService
import ngoctv.vn.todoapp.data.services.TaskService
import ngoctv.vn.todoapp.utils.*

@Suppress("UNCHECKED_CAST")
class AddTaskViewModel(private val taskService: TaskService, private val authService: AuthService): ViewModel(){
    val showError = MutableLiveData<Boolean>().apply{
        value = false
    }
    val name = MutableLiveData<String>().apply{
        value = ""
    }
    val description = MutableLiveData<String>().apply{
        value = ""
    }
    val address = MutableLiveData<String>().apply{
        value = ""
    }
    val date = MutableLiveData<String>().apply{
        value = currentDateString()
    }
    val time = MutableLiveData<String>().apply{
        value = currentTimeString()
    }

    val nameError: LiveData<String?> = Transformations.map(name){name ->
        validateName(name)
    }
    val descriptionError: LiveData<String?> = Transformations.map(description){ description: String? ->
        validateDescription(description)
    }
    val addressError: LiveData<String?> = Transformations.map(address){ address: String? ->
        validateAddress(address)
    }
    val dateError: LiveData<String?> = Transformations.map(date){ date: String? ->
        validateDate(date)
    }
    val timeError: LiveData<String?> = Transformations.map(time){ time: String? ->
        validateTime(time)
    }

    val inputValid: LiveData<Boolean> = MediatorLiveData<Boolean>().apply{
        addSource(nameError){value = validateInput()}
        addSource(descriptionError){value = validateInput()}
        addSource(addressError){value = validateInput()}
        addSource(dateError){value = validateInput()}
        addSource(timeError){value = validateInput()}
    }

    fun add(onComplete: ((String) -> Unit)?){
        showError.value = true
        val inputValid = validateInput()
        val currentUser: User = authService.requireCurrentUser()
        when{
            !inputValid -> onComplete?.invoke("Thêm công việc thất bại: Đầu vào không hợp lệ")
            else -> taskService.saveTask(
                Task(
                    name.value!!,
                    description.value!!,
                    address.value!!,
                    date.value!!,
                    time.value!!,
                    currentUser.username
                )
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onComplete?.invoke("Thêm công việc thành công")
                    clearTexts()
                }, {
                    onComplete?.invoke("Thêm công việc thất bại: " + it.message)
                })
        }
    }

    private fun validateInput(): Boolean {
        return nameError.value == null
        && descriptionError.value == null
        && addressError.value == null
        && dateError.value == null
        && timeError.value == null
    }

    private fun clearTexts() {
        showError.value = false
        name.value = ""
        description.value = ""
        address.value = ""
        date.value = ""
        time.value = ""
    }

    class Factory(private val taskService: TaskService, private val authService: AuthService): ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AddTaskViewModel(taskService, authService) as T
        }
    }
}