package ngoctv.vn.todoapp.ui.views

import android.view.View
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout


@BindingAdapter("isGone")
fun setGone(view: View, isGone: Boolean?){
    if(isGone == true)
        view.visibility = View.GONE
    else if(isGone == false)
        view.visibility = View.VISIBLE
}
@BindingAdapter("isVisible")
fun setVisible(view: View, isVisible: Boolean?){
    if(isVisible == true)
        view.visibility = View.VISIBLE
    else if(isVisible == false)
        view.visibility = View.GONE
}
@BindingAdapter("inputError")
fun setInputError(inputLayout: TextInputLayout, error: String?){
    inputLayout.error = error
}