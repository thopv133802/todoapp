package ngoctv.vn.todoapp.ui.views.auth

import androidx.databinding.BindingAdapter
import com.google.android.material.button.MaterialButton
import ngoctv.vn.todoapp.R

@BindingAdapter("authButtonText")
fun setAuthButtonText(button: MaterialButton, userExist: Boolean?){
    if(userExist == true){
        button.text = button.resources.getString(R.string.login)
    }
    else if(userExist == false){
        button.text = button.resources.getString(R.string.register)
    }
    else
        button.text = ""
}