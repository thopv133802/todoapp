package ngoctv.vn.todoapp.ui.views.tasks

import android.graphics.Color
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import ngoctv.vn.todoapp.R

@BindingAdapter("doneIcon")
fun setDoneIcon(imageView: ImageView, isDone: Boolean?){
    if(isDone == true){
        imageView.setImageDrawable(imageView.resources.getDrawable(R.drawable.ic_done_black_24dp))
    }
    else if(isDone == false){
        imageView.setImageDrawable(imageView.resources.getDrawable(R.drawable.ic_close_black_24dp))
    }
}
@BindingAdapter("doneStatusLabelColor")
fun setDoneStatusLAbelColor(textView: TextView, isDone: Boolean?){
    if(isDone == true){
        textView.setTextColor(Color.GREEN)
    }
    else if(isDone == false)
        textView.setTextColor(Color.RED)
}