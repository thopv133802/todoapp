package ngoctv.vn.todoapp.ui.viewModels.tasks

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import ngoctv.vn.todoapp.data.models.TasksTaskModel
import ngoctv.vn.todoapp.data.services.TaskService
import ngoctv.vn.todoapp.utils.toDatetimeString

class TasksTaskViewModel(private val taskService: TaskService) : ViewModel(){
    val task: MutableLiveData<TasksTaskModel> = MutableLiveData()

    val address: LiveData<String> = Transformations.map(task){task ->
        "Địa điểm: " + task.address
    }
    val deadline: LiveData<String> = Transformations.map(task){task ->
        "Thời điểm: " + task.deadline.toDatetimeString()
    }

    fun setTask(task: TasksTaskModel) {
        this.task.value = task
    }

    fun done(onSuccess: ((String) -> Unit)?, onFail: ((String) -> Unit)?, onFinally: (() -> Unit)?) {
        val task = this.task.value
        when (task) {
            null -> onFail?.invoke("Task hiện tại không khả dụng")
            else -> {
                taskService.setDone(task.id)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnTerminate{
                        onFinally?.invoke()
                    }
                    .subscribe({
                        onSuccess?.invoke("Cập nhật trạng thái thành công")
                    }, {error ->
                        onFail?.invoke("Cập nhật trạng thái thất bại: " + error.message)
                    })
            }
        }
    }
    fun done(){
        this.task.value?.let{task ->
            taskService.setDone(task.id, !task.isDone)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val taskService: TaskService): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return TasksTaskViewModel(taskService) as T
        }
    }
}