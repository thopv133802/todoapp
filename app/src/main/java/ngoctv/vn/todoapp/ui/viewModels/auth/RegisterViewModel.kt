package ngoctv.vn.todoapp.ui.viewModels.auth

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import ngoctv.vn.todoapp.data.services.AuthService
import java.lang.Exception

@Suppress("UNCHECKED_CAST")
class RegisterViewModel(private val authService: AuthService): ViewModel(){
    var showError: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply{
        value = false
    }

    val username: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = ""
    }
    val password: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = ""
    }
    val confirmPassword: MutableLiveData<String> = MutableLiveData<String>().apply{
        value = ""
    }

    val usernameError: LiveData<String?> = Transformations.map(username){username ->
        validateUsername(username)
    }

    private fun validateUsername(username: String?): String? {
        return when{
            username.isNullOrBlank() -> "Tài khoản không được để trống"
            username.length < 4 -> "Tài khoản phải có ít nhất 4 ký tự"
            else -> null
        }
    }

    val passwordError: LiveData<String?> = Transformations.map(password){password ->
        validatePassword(password)
    }

    private fun validatePassword(password: String?): String? {
        return when{
            password.isNullOrBlank() -> "Mật khẩu không được để trống"
            password.length < 6 -> "Mật khẩu phải có ít nhất 6 ký tự"
            else -> null
        }
    }

    val confirmPasswordError: LiveData<String> = MediatorLiveData<String>().apply{
        addSource(password){
            value = validateConfirmPassword()
        }
        addSource(confirmPassword){
            value = validateConfirmPassword()
        }
    }
    private fun validateConfirmPassword(): String?{
        return if(password.value != confirmPassword.value) "Nhập lại mật khẩu chưa trùng khớp" else null
    }

    val inputValid: LiveData<Boolean> = MediatorLiveData<Boolean>().apply{
        value = false
        addSource(usernameError){
            value = validateInput()
        }
        addSource(passwordError){
            value = validateInput()
        }
        addSource(confirmPasswordError){
            value = validateInput()
        }
    }

    private fun validateInput(): Boolean {
        return usernameError.value == null && passwordError.value == null
    }

    fun register(onSuccess: ((String) -> Unit)?, onFail: ((String) -> Unit)?){
        showError.value = true
        val usernameError = validateUsername(username.value) != null
        val passwordError = validatePassword(password.value) != null
        val confirmPasswordError = validateConfirmPassword() != null
        when{
            usernameError -> onFail?.invoke("Tài khoản không hợp lệ")
            passwordError -> onFail?.invoke("Mật khẩu không hợp lệ")
            confirmPasswordError -> onFail?.invoke("Nhập lại mật khẩu chưa hợp lệ")
            else -> {
                authService.register(
                    username.value!!,
                    password.value!!
                ).observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        onSuccess?.invoke("Đăng ký thành công")
                    }, {
                        onFail?.invoke("Đăng ký thất bại: " + it.message)
                    })
            }
        }
    }

    class Factory(private val authService: AuthService): ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RegisterViewModel(authService) as T
        }
    }
}