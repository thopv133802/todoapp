package ngoctv.vn.todoapp.ui.views.tasks

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import ngoctv.vn.todoapp.R
import ngoctv.vn.todoapp.data.entities.Task

class TaskFilterPopupMenu(context: Context, anchorView: View) {
    private val popupMenu: PopupMenu = PopupMenu(context, anchorView, Gravity.END)

    init{
        val menu = popupMenu.menu
        menu.add(0, 0, 0, "Không lọc")
        menu.add(0, 1, 1, "Hoàn thành")
        menu.add(0, 2, 2, "Chưa làm")
    }

    fun show(onCompleted: ((Task.Filter) -> Unit)?) {
        popupMenu.setOnMenuItemClickListener {item ->
            when(item.itemId){
                0 -> onCompleted?.invoke(Task.Filter.NoFilter)
                1 -> onCompleted?.invoke(Task.Filter.Done)
                2 -> onCompleted?.invoke(Task.Filter.Todo)
            }
            true
        }
        popupMenu.show()
    }
}
