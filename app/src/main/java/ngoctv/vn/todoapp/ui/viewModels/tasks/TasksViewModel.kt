package ngoctv.vn.todoapp.ui.viewModels.tasks

import androidx.lifecycle.*
import androidx.paging.PagedList
import ngoctv.vn.todoapp.data.entities.Task
import ngoctv.vn.todoapp.data.entities.User
import ngoctv.vn.todoapp.data.models.TasksTaskModel
import ngoctv.vn.todoapp.data.services.AuthService
import ngoctv.vn.todoapp.data.services.TaskService
import ngoctv.vn.todoapp.utils.TASKS_PAGE_SIZE

class TasksViewModel(private val authService: AuthService, private val taskService: TaskService): ViewModel(){
    private val filter = MutableLiveData<Task.Filter>().apply{
        value = Task.Filter.NoFilter
    }
    fun setFilter(filter: Task.Filter) {
        this.filter.value = filter
    }

    private val currentUser: User = authService.requireCurrentUser()
    val tasks: LiveData<PagedList<TasksTaskModel>> = Transformations.switchMap(filter){filter ->
        taskService.fetchLiveTasks(currentUser.username, TASKS_PAGE_SIZE, filter)
    }

    val anyTasks: LiveData<Boolean> = Transformations.map(tasks){tasks ->
        !tasks.isNullOrEmpty()
    }
    val taskStatistics: LiveData<String> = MediatorLiveData<String>().apply{
        addSource(tasks){
            value = generateTaskStatistics()
        }
        addSource(filter){
            value = generateTaskStatistics()
        }
    }

    private fun generateTaskStatistics(): String? {
        if(filter.value == null || tasks.value.isNullOrEmpty()) return null

        val filter: Task.Filter = filter.value!!
        val tasks: PagedList<TasksTaskModel> = tasks.value!!

        val completedCount = tasks.count { task -> task.isDone }
        val notCompletedCount = tasks.size - completedCount

        return when(filter){
            Task.Filter.NoFilter -> {
                "Toàn bộ công việc\n" + "Tổng cộng ${tasks.size}: " + "Hoàn thành $completedCount" + ", Chưa hoàn thành $notCompletedCount"
            }
            Task.Filter.Done -> {
                "Công việc đã hoàn thành\n" +  "Hoàn thành tổng cộng $completedCount công việc"
            }
            Task.Filter.Todo -> {
                "Công việc chưa hoàn thành\n" + "Chưa toàn hành tổng cộng $notCompletedCount công việc"
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val authService: AuthService, private val taskService: TaskService):
        ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return TasksViewModel(authService, taskService) as T
        }
    }
}