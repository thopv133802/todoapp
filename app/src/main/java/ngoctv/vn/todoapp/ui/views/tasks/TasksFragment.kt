package ngoctv.vn.todoapp.ui.views.tasks


import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment.findNavController

import ngoctv.vn.todoapp.R
import ngoctv.vn.todoapp.databinding.TasksFragmentBinding
import ngoctv.vn.todoapp.ui.viewModels.tasks.TasksViewModel
import ngoctv.vn.todoapp.utils.InjectorUtil

class TasksFragment : Fragment() {
    lateinit var binding: TasksFragmentBinding
    lateinit var viewModel: TasksViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val login = InjectorUtil.authService(requireContext())
            .userIsLogged()
        if(!login)
            findNavController(this).navigate(R.id.loginFragment)
        else {
            initViewModel()
            initBInding(inflater, container)
            setupViews()

            return binding.root
        }
        return null
    }

    private fun validateLoginStatus() {

    }

    private fun setupViews() {
        val tasksAdapter = TasksAdapter(findNavController(this), viewLifecycleOwner)
        binding.tasks.adapter = tasksAdapter
        viewModel.tasks.observe(viewLifecycleOwner, Observer{tasks ->
            tasksAdapter.submitList(tasks)
        })
        binding.addTask.setOnClickListener {
            findNavController(this).navigate(R.id.action_tasksFragment_to_addTaskFragment)
        }
    }

    private fun initBInding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, R.layout.tasks_fragment, container, false)
        binding.setLifecycleOwner(viewLifecycleOwner)
        binding.viewModel = viewModel
    }

    private fun initViewModel() {
        val factory = InjectorUtil.tasksViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(this, factory).get(TasksViewModel::class.java)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.action_filter){
            val filterPopupMenu = TaskFilterPopupMenu(requireContext(), requireActivity().findViewById(R.id.action_filter))
            filterPopupMenu.show{filter ->
                viewModel.setFilter(filter)
            }
        }
        else if(item.itemId == R.id.action_logout){
            InjectorUtil.authService(requireContext())
                .logout()
            findNavController(this).navigate(R.id.loginFragment)
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.tasks_fragment, menu)
    }
}
