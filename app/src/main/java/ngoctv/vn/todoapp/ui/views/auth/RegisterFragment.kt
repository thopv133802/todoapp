package ngoctv.vn.todoapp.ui.views.auth


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import ngoctv.vn.todoapp.R
import ngoctv.vn.todoapp.databinding.RegisterFragmentBinding
import ngoctv.vn.todoapp.ui.viewModels.auth.RegisterViewModel
import ngoctv.vn.todoapp.utils.InjectorUtil

class RegisterFragment : Fragment() {
    lateinit var binding: RegisterFragmentBinding
    lateinit var viewModel: RegisterViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        initViewModel()
        initBinding(inflater, container)
        setupViews()
        return binding.root
    }

    private fun setupViews() {
        binding.register.setOnClickListener {
            viewModel.register({message ->
                showMessage(message)
            }, {error ->
                showMessage(error)
            })
        }
    }
    private fun showMessage(message: String){
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
            .show()
    }

    private fun initViewModel() {
        val factory = InjectorUtil.registerViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(this, factory).get(RegisterViewModel::class.java)
    }

    private fun initBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, R.layout.register_fragment, container, false)
        binding.viewModel = viewModel
        binding.setLifecycleOwner(viewLifecycleOwner)
    }
}
